package com.vivaladev.comivoyager.entity;

import java.util.ArrayList;
import java.util.List;

public class Node {
    public int id;
    public List<Node> connections;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Node> getConnections() {
        return connections;
    }

    public void setConnections(List<Node> connections) {
        this.connections = connections;
    }

    public Node(int id) {
        this.id = id;
        connections = new ArrayList<>();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + connections.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Node other = (Node) obj;

        if (id != other.id) {
            return false;
        }

        if (!connections.equals(other.connections)) {
            return false;
        }

        return true;
    }
}
