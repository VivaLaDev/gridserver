package com.vivaladev.comivoyager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    private String resultPath;
    private int resultNumber;

    public boolean isLower(Result with){
        return this.getResultNumber() < with.getResultNumber();
    }
}
