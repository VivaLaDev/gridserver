package com.vivaladev.comivoyager.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Vertex {

    private int label;
    private boolean isVisited;
    private int row;
    private int column;
    private int value;
    private Boolean start;
}
