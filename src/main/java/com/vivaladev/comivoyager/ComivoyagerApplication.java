package com.vivaladev.comivoyager;

import com.vivaladev.comivoyager.entity.Result;
import com.vivaladev.comivoyager.entity.Vertex;
import com.vivaladev.comivoyager.entity.json.Task;
import com.vivaladev.comivoyager.util.BashUtil;
import com.vivaladev.comivoyager.util.GraphUtils;
import com.vivaladev.comivoyager.util.HamiltonianPaths;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@EnableScheduling
@SpringBootApplication
public class ComivoyagerApplication {

    private static int taskCount = 0;
    private List<Result> results = new ArrayList<>();
    private static String directory;
    private static List<Vertex> vertexList = new ArrayList<>();
    private static int numberOfNodes;

    public static void main(String[] args) {
        SpringApplication.run(ComivoyagerApplication.class, args);
        try {
            Path currentRelativePath = Paths.get("");
            directory = currentRelativePath.toAbsolutePath().toString();
            System.out.println("Current relative path is: " + directory);
            startJob();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("OOPS, SOMETHING WENT WRONG, GOODBYE ENAKIN");
            System.exit(1);
        }
    }

    private static void startJob() throws Exception {
        int[][] graph = getGraph();
        List<Task> taskCommand = createTaskCommand(graph);
        taskCommand
                .forEach(ComivoyagerApplication::createTaskFile);
        prepareAndRunBash();
    }

    private static List<Task> createTaskCommand(int[][] graph) {
        String graphVertexMatrix = GraphUtils.changeMatrixType(vertexList);
        List<List<String>> paths = prepareTaskPaths(graph);
        return paths
                .stream()
                .map(listPaths -> {
                    Task task = new Task();
                    task.setText(createTaskText(numberOfNodes, graphVertexMatrix, listPaths));
                    task.setNumberOfTask(taskCount);
                    taskCount++;
                    return task;
                })
                .collect(Collectors.toList());
    }

    private static List<List<String>> prepareTaskPaths(int[][] graph) {
        List<String> paths = HamiltonianPaths.collectPaths(graph, numberOfNodes);
        List<String> filteredList = paths.stream().distinct().collect(Collectors.toList());
        Collections.shuffle(filteredList);
        Collection<List<String>> lists = divideListOnPages(filteredList, numberOfNodes * 3);
        return new ArrayList<>(lists);
    }

    private static Collection<List<String>> divideListOnPages(List<String> reports, int countRow) {
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        return reports
                .stream()
                .collect(Collectors.groupingBy(report -> atomicInteger.getAndIncrement() / countRow))
                .values();
    }

    private static String createTaskText(int countOfNodes, String matrix, List<String> paths) {
        StringBuilder builder = new StringBuilder();
        builder.append(countOfNodes)
                .append("\n")
                .append(matrix)
                .append("\n");
        paths.forEach(s -> builder.append(s).append("\n"));
        return builder.toString();
    }

    private static void createFile(File file) {
        try {
            if (file.createNewFile()) {
                System.out.println("Successfully created " + file.getName() + " file, with path: " + file.getAbsolutePath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createTaskFile(Task task) {
        String taskText = task.getText();
        int number = task.getNumberOfTask();
        if (taskText == null || taskText.isEmpty()) {
            System.out.println("Got empty task with num " + number);
            return;
        }
        File file = new File(directory + "/graph-" + number + ".txt");
        File outFile = new File(directory + "/output-" + number + ".txt");

        deleteIfExist(file);
        deleteIfExist(outFile);

        createFile(file);
        createFile(outFile);

        try (FileWriter fileWriter = new FileWriter(file, false)) {
            fileWriter.write(taskText);
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        taskCount++;
    }

    private static void prepareAndRunBash() {
        String command = "./mygrid setgrid ../peerconfig.gdf; " +
                "./mygrid addjob ../job.jdf; " +
                "./mygrid status; " +
                "./mygrid waitforjob 1";
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BashUtil.executeBashCommand(command);
    }

    private static int[][] getGraph() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(directory + "/graph.txt"));
        System.out.println("Get number of nodes");
        numberOfNodes = scanner.nextInt();
        System.out.println("Graph has: " + numberOfNodes + " nodes");
        int[][] graph = new int[numberOfNodes][numberOfNodes];
        int vertexNum = 1;
        for (int sourceVertex = 0; sourceVertex <= numberOfNodes - 1; sourceVertex++) {
            for (int destinationVertex = 0; destinationVertex <= numberOfNodes - 1; destinationVertex++) {
                int val = scanner.nextInt();
                graph[sourceVertex][destinationVertex] = val;
                Vertex vertex = new Vertex();
                vertex.setLabel(vertexNum);
                vertex.setVisited(false);
                vertex.setRow(sourceVertex + 1);
                vertex.setColumn(destinationVertex + 1);
                vertex.setValue(val);
                vertexList.add(vertex);
                vertexNum++;
            }
        }

        System.out.println("Graph matrix was loaded from graph.txt");
        return graph;
    }

    @Scheduled(cron = "0/15 * * * * *")
    public void checkResult() throws FileNotFoundException {
        System.out.println("Started the result finder");
        Scanner sc;
        for (int i = 0; i <= taskCount; i++) {
            String filename = "output-" + i + ".txt";
            File file = new File(filename);
            if (file.exists()) {
                System.out.println("check for file " + filename);
                sc = new Scanner(file);
                if (sc.hasNext()) {
                    Result result = new Result(sc.next(), sc.nextInt());
                    results.add(result);
                }
            }
        }
        if (results.size() < 1) {
            throw new RuntimeException("haven't results to collect");
        }
        Result minRes = results.get(0);
        for (int i = 1; i < results.size(); i++) {
            Result curRes = results.get(i);
            if (!minRes.isLower(curRes)) {
                minRes.setResultNumber(curRes.getResultNumber());
                minRes.setResultPath(curRes.getResultPath());
            }
        }
        System.out.println("Min path have length: " + minRes.getResultNumber() + " and path " + minRes.getResultPath().replaceAll("_", " "));
    }

    private static void deleteIfExist(File file) {
        if (file.exists()) {
            file.delete();
        }
    }
}
