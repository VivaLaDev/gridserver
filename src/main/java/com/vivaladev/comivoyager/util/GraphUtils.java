package com.vivaladev.comivoyager.util;

import com.vivaladev.comivoyager.entity.Vertex;

import java.util.List;

public class GraphUtils {

    public static String changeMatrixType(List<Vertex> graph) {
        StringBuilder str = new StringBuilder();
        graph.forEach(vertex -> str.append(vertex.getColumn())
                .append(" ")
                .append(vertex.getRow())
                .append(" ")
                .append(vertex.getValue())
                .append(" \n"));
        return str.toString();
    }

    public static boolean isExistPath(Vertex start, Vertex finish, int[][] matrix) {
        int i = matrix[start.getRow()][finish.getRow()];
        return 0 != i;
    }
}
