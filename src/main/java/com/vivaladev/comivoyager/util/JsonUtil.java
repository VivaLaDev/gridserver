package com.vivaladev.comivoyager.util;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JsonUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String object2JsonString(Object obj) throws IOException {
        return objectMapper.writeValueAsString(obj);
    }

    public static void writeObject2FileAsJson(Object obj, String filename) throws IOException {
        File file = new File(filename);
        if (file.exists()) {
            objectMapper.writeValue(file, obj);
        } else {
            file.createNewFile();
            objectMapper.writeValue(file, obj);
        }
    }
}
