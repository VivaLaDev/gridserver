package com.vivaladev.comivoyager.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HamiltonianPaths {

    private static List<String> paths = new ArrayList<>();

    private static void printAllHamiltonianPaths(Graph g, int v,
                                                 boolean[] visited,
                                                 List<Integer> path, int N) {
        // if all the vertices are visited, then
        // hamiltonian path exists
        if (path.size() == N) {
            StringBuilder pathRes = new StringBuilder();
            // print hamiltonian path
            for (int i : path) {
                pathRes.append(i).append(" ");
            }
            Scanner sc = new Scanner(pathRes.toString());
            pathRes.append(sc.next());
            paths.add(preparePath(pathRes));
            return;
        }

        // Check if every edge starting from vertex v leads
        // to a solution or not
        for (int w : g.adjList.get(v)) {
            // process only unvisited vertices as Hamiltonian
            // path visits each vertex exactly once
            if (!visited[w]) {
                visited[w] = true;
                path.add(w);

                // check if adding vertex w to the path leads
                // to solution or not
                printAllHamiltonianPaths(g, w, visited, path, N);

                // Backtrack
                visited[w] = false;
                path.remove(path.size() - 1);
            }
        }
    }

    private static String preparePath(StringBuilder path) {
        path.insert(0, "p:");
        return path.toString().replaceAll(" ", "_");
    }

    private static List<String> getPaths() {
        return paths;
    }

    public static List<String> collectPaths(int[][] matrix, int numberOfNodes) {
        // vector of graph edges as per above diagram
        List<Edge> edges = getEdges(matrix);

        // Set number of vertices in the graph

        // create a graph from edges
        Graph g = new Graph(edges, numberOfNodes);

        // starting node
        int start = 0;

        // add starting node to the path
        List<Integer> path = new ArrayList<>();
        path.add(start);

        // mark start node as visited
        boolean[] visited = new boolean[numberOfNodes];
        visited[start] = true;

        printAllHamiltonianPaths(g, start, visited, path, numberOfNodes);
        return getPaths();
    }

    private static List<Edge> getEdges(int[][] matrix) {
        List<Edge> edgeList = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            List<Integer> possibleWay = getPossibleWay(matrix, i);
            int finalI = i;
            possibleWay.forEach(integer -> edgeList.add(new Edge(finalI, integer)));
        }
        return edgeList;
    }

    private static List<Integer> getPossibleWay(int[][] graph, int currentNode) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < graph.length; i++) {
            if (graph[currentNode][i] != 0) {
                list.add(i);
            }
        }
        return list;
    }
}
