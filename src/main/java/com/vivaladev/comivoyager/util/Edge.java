package com.vivaladev.comivoyager.util;

// data structure to store graph edges
class Edge {
    int source, dest;

    public Edge(int source, int dest) {
        this.source = source;
        this.dest = dest;
    }
}
